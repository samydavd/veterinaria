const methods = {
	arrayResponse: function(response) {
        let respuestas =  [];
        let esErrorValidacion, data;

        if (typeof response === 'string') {
            respuestas = [response];
        }
        else if (Array.isArray(response)) {
            respuestas = response;
        }
        else if (typeof response === 'object') {
            if(response.hasOwnProperty('data')){
                let propiedades = Object.getOwnPropertyNames(response.data);
                propiedades.forEach((elemento) => {
                    respuestas.push(Object.getOwnPropertyDescriptor(response.data, elemento).value);
                });
            }
            else if(response.hasOwnProperty('response') && response.response.hasOwnProperty('data') ){
                data = response.response.data;

                if (typeof data === 'string') {
                    respuestas = [data];
                }
                else if (Array.isArray(data)) {
                    respuestas  = data;
                }
                else if (typeof data === 'object') {
                    if ('errors' in data) {
                        if (typeof data.errors === 'string') {
                            respuestas = [data.errors];
                        }
                        else {
                            Object.keys(data.errors).forEach((key) => {
                                respuestas.push(data.errors[key][0]);
                            });
                        }
                    }
                    else if ('error' in data) {
                        if (typeof data.error === 'string') {
                            respuestas = [data.error];
                        }
                        else {
                            Object.keys(data.error).forEach((key) => {
                                respuestas.push(data.error[key][0]);
                            });
                        }
                    }
                    else if ('message' in data) {
                        if (data.message) {
                            respuestas.push(data.message);
                        }
                        else {
                            respuestas.push(data.exception.split("\\").pop());
                        }
                    }
                    else {
                        for (let error in data) {
                            if (data[error]) {
                                if (typeof data[error] == 'string') {
                                    if (!data[error].match(/Illuminate|\/vendor\//)) {
                                        respuestas.push(data[error]);
                                    }
                                }
                                else if (Array.isArray(data[error])) {
                                    for (let key in data[error]) {
                                        respuestas.push(data[error][key]);
                                    }
                                }
                                else if (typeof data[error] === 'object') {
                                    Object.keys(data[error]).forEach((key) => {
                                        var elemento = data[error][key];

                                        if (elemento.file && !elemento.file.match(/\/vendor\/|\/Middleware\/|index.php/)) {
                                            if (elemento.function && elemento.function == '__callStatic' && elemento.line) {
                                                let archivo = elemento.file.split('/').pop();
                                                respuestas.push(archivo + ': ' + elemento.line);
                                            }
                                            else if (typeof elemento !== 'object') {
                                                respuestas.push(elemento);
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    };
                }
                else {
                    respuestas = ['Se ha generado un error inesperado. Por favor contacte a soporte.'];
                }
            }
            else if (response.hasOwnProperty('mensaje') || 'message' in response) {
                respuestas = [response.message];
            }
        }

        if (respuestas[0] == 'HttpException') {
            window.location.href = '/login';
        }
        else {
            return respuestas;
        }
    },
}

export default methods;