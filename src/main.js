import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';

// Librerias
import 'bootstrap'; 
import axios from 'axios';
import VueAxios from 'vue-axios';
import Datatables from 'datatables';
import VueSweetalert2 from 'vue-sweetalert2';
import Loading from 'vue-loading-overlay';
import methods from '_config/methods';
import OptionsSweetalert2 from '_config/sweetalert2';

// Estilos
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'vue-loading-overlay/dist/vue-loading.css';
import 'datatables/media/css/jquery.dataTables.min.css';

// Global jQuery

window.$ = window.jQuery = require('jquery');

// Global Loader
window.loading = function(self, ...args) {
    return self.$loading.show({
        loader: 'spinner',
        opacity: 0.4,
        backgroundColor: '#4C4C4C',
        zIndex: 9999,
        ...args[0]
    });
};

// Rutas
const routes = [
    {
        name: 'home',
        path: '/',
        component: () => import( /* webpackChunkName: 'components/Personas' */ '_views/Personas'),
    },
    {
        name: 'razas',
        path: '/razas',
        component: () => import( /* webpackChunkName: 'components/Razas' */ '_views/Razas'),
    },
    {
        name: 'mascotas',
        path: '/mascotas',
        component: () => import( /* webpackChunkName: 'components/Razas' */ '_views/Mascotas'),
    }
];

const router = new VueRouter({mode : 'history', routes});

Vue.config.productionTip = false;
Vue.use(Loading);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueSweetalert2, OptionsSweetalert2);

// Componentes
Vue.component('alertas', () => import('_comp/Alertas'));
Vue.component('modal', () => import('_comp/Modal'));
Vue.component('sidebar', () => import('_ly/Sidebar'));


// API URL

let url = process.env.NODE_ENV == 'development' ?  'http://localhost/api-veterinaria/' : 'isnotjs.com/app-remesas/api/';

console.log(url);

new Vue({
  router,
  methods,
  data() {
  	return {
    	app_url : url,
  	}
  },
  render: h => h(App),
}).$mount('#app')
