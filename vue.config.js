let path = require('path');
// vue.config.js
module.exports = {
    configureWebpack: {
        resolve: {
            extensions: ['.js', '.vue'],
            alias: {
                '_comp': path.join(__dirname, 'src/components'),
                '_views': path.join(__dirname, 'src/views'),
                '_ly': path.join(__dirname, 'src/layout'),
                '_config': path.join(__dirname, 'src/config'),
            },
        }
    }
}